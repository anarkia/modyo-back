package com.modyo.test.api.models;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Ability {
	
	@JsonProperty("ability")
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<GeneralType> ability;
	
	private boolean is_hidden;
	
	private int slot;
	
	public List<GeneralType> getAbility() {
		return ability;
	}
	public void setAbility(List<GeneralType> ability) {
		this.ability = ability;
	}
	public boolean isIs_hidden() {
		return is_hidden;
	}
	public void setIs_hidden(boolean is_hidden) {
		this.is_hidden = is_hidden;
	}
	public int getSlot() {
		return slot;
	}
	public void setSlot(int slot) {
		this.slot = slot;
	}

}
