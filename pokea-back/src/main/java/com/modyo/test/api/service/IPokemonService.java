package com.modyo.test.api.service;

import java.util.List;

import org.springframework.http.HttpEntity;

import com.modyo.test.api.models.Evolution;
import com.modyo.test.api.models.Pokemon;



public interface IPokemonService {
	
	public List<Pokemon> findAll(int page);
	
	public Pokemon getPokemon(String param);
	
	public HttpEntity<String> createHttp();
	
	public String getDescription(String param);
	
	public List<String> getEvolution(String param);

}
