package com.modyo.test.api.models;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Evolution {
	
	@JsonProperty("species")
	private GeneralType species;
	
	@JsonProperty("evolves_to")
	private List<Evolution> evolves_to;

	public GeneralType getSpecies() {
		return species;
	}

	public void setSpecies(GeneralType species) {
		this.species = species;
	}

	public List<Evolution> getEvolves_to() {
		return evolves_to;
	}

	public void setEvolves_to(List<Evolution> evolves_to) {
		this.evolves_to = evolves_to;
	}
	
	
	//@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
}
