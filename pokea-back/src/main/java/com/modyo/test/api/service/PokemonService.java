package com.modyo.test.api.service;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.modyo.test.api.models.Evolution;
import com.modyo.test.api.models.Pokemon;

@Service
public class PokemonService implements IPokemonService {
	
	@Autowired
	private RestTemplate restTemplate;
	
	private HttpEntity<String> entity = createHttp();

	public String calculateUrl(int page) {
		String url = "https://pokeapi.co/api/v2/pokemon?limit=";
		url += "20";
		url += "&offset=";
		url += String.valueOf(20*(page-1));
		return url;
	}
	
	@Override
	public List<Pokemon> findAll(int page) {
		
		List<Pokemon> pokePaginator = new ArrayList<Pokemon>();
		
		String url = calculateUrl(page);
		
		ResponseEntity<String> response = restTemplate.exchange(url,  HttpMethod.GET, entity, String.class);
		
		try {
			JSONArray json = (JSONArray) new JSONObject(response.getBody()).get("results");
			
			for (int i = 0; i < json.length(); i++) {
			    try {
			        JSONObject jsonObject = json.getJSONObject(i);
			        pokePaginator.add(getPokemon(jsonObject.getString("name")));
			    } catch (JSONException e) {
			        e.printStackTrace();
			    }
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pokePaginator;
	}

	@Override
	public Pokemon getPokemon(String param) {
		ResponseEntity<Pokemon> pokemon = restTemplate.exchange("https://pokeapi.co/api/v2/pokemon/"+ param,  HttpMethod.GET, entity, Pokemon.class);
		return pokemon.getBody();
	}

	@Override
	public HttpEntity<String> createHttp() {
		HttpHeaders headers = new HttpHeaders();
		headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
		
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
		return entity;
	}

	@Override
	public String getDescription(String param) {
		ResponseEntity<String> pokemon = restTemplate.exchange("https://pokeapi.co/api/v2/pokemon-species/"+ param,  HttpMethod.GET, entity, String.class);
		try {
			String description =new JSONObject(pokemon.getBody()).getJSONArray("flavor_text_entries").get(0).toString();
			description  =new JSONObject(description).get("flavor_text").toString().trim();
			return description;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<String> getEvolution(String param) {
		ResponseEntity<String> pokemon = restTemplate.exchange("https://pokeapi.co/api/v2/pokemon-species/"+ param,  HttpMethod.GET, entity, String.class);
		List<String> nameEvolutions = new ArrayList<String>();
		try {
			String evolution =new JSONObject(pokemon.getBody()).getJSONObject("evolution_chain").get("url").toString();
			
			pokemon = restTemplate.exchange(evolution,  HttpMethod.GET, entity, String.class);
			
			evolution =new JSONObject(pokemon.getBody()).get("chain").toString();
			Gson gson = new Gson();
			
			Evolution evolutionModel = gson.fromJson(evolution, Evolution.class);
			nameEvolutions.add(evolutionModel.getSpecies().getName());
			while(evolutionModel.getEvolves_to().size() > 0){
				evolutionModel = evolutionModel.getEvolves_to().get(0);
				nameEvolutions.add(evolutionModel.getSpecies().getName());
			}
			
			return nameEvolutions;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

}
