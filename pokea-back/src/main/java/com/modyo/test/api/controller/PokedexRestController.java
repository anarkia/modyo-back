package com.modyo.test.api.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.modyo.test.api.models.Evolution;
import com.modyo.test.api.models.Pokemon;
import com.modyo.test.api.service.IPokemonService;


@RestController
@RequestMapping("/api/pokedex")
public class PokedexRestController {

	@Autowired
	private IPokemonService iPokemonService;
	
	
	@GetMapping("/pokemons/page/{page}")
	public ResponseEntity<?> getAllPokemons(@PathVariable Integer page) {
		Map<String, Object> response = new HashMap<>();
		try {
			List<Pokemon> allPokemons = iPokemonService.findAll(page);
			response.put("listPokemons",allPokemons);
		}catch(Exception e) {
			response.put("mensaje", "Error al realizar la peticion");
			response.put("error", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<Object>(response, HttpStatus.OK); 
	}
	
	@GetMapping("/pokemons/detail/{param}")
	public ResponseEntity<?> getPokemon(@PathVariable String param) {
		Map<String, Object> response = new HashMap<>();
		try {
			response.put("pokemon",iPokemonService.getPokemon(param));
		}catch(Exception e) {
			response.put("mensaje", "Error al realizar la peticion");
			response.put("error", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Object>(response, HttpStatus.OK); 
	}
	
	@GetMapping("/pokemons/detail/evolution/{param}")
	public List<String> getEvolvesTo(@PathVariable String param) {
		return iPokemonService.getEvolution(param);
	}
	
	
	@GetMapping("/pokemons/detail/description/{param}")
	public String getDescription(@PathVariable String param) {
		return iPokemonService.getDescription(param);
	}
}
