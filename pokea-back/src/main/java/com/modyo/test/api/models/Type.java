package com.modyo.test.api.models;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Type {

	private int slot;
	
	@JsonProperty("type")
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<GeneralType> type;
	
	public List<GeneralType> getType() {
		return type;
	}
	public void setType(List<GeneralType> type) {
		this.type = type;
	}
	
	public int getSlot() {
		return slot;
	}
	public void setSlot(int slot) {
		this.slot = slot;
	}
	
}
