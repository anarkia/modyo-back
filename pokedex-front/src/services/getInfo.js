import axios from "axios";

const POKEDEX_API_URL = "http://localhost:8080/api/pokedex/pokemons/detail/";

class getInfo {
    getDescription(id){
        return axios.get(POKEDEX_API_URL + "description/" + id, {
            'headers': {
            }
        });
    }
    getEvolution(id){
        return axios.get(POKEDEX_API_URL + "evolution/" + id, {
            'headers': {
            }
        });
    }
}

export default new getInfo();