import axios from "axios";

const POKEDEX_API_URL = "http://localhost:8080/api/pokedex/pokemons";
const CONTROLADOR = "/page/";

class getAllpokemon{
    getPokemon(page){
        return axios.get(POKEDEX_API_URL + CONTROLADOR + page, {'headers':{    
        }});
    }
}

export default new getAllpokemon();